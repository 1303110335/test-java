package holding;

import java.util.*;
import net.mindview.util.*;

import static util.Common.print;

public class UniqueWords {
    public static void main(String[] args) {
//        Set<String> words = new TreeSet<String>(new TextFile("src/main/java/holding/SetOperations.java", "\\W+"));

        Set<String> words = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
        words.addAll(new TextFile("src/main/java/holding/UniqueWords.java", "\\W+"));

        //aeiou
        Set<Character> vowels = new HashSet<Character>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));
        int num = 0;
        for(String w : words) {
            int temp = 0;
            for(char c : w.toCharArray()) {
                if (vowels.contains(c)) {
                    temp ++;
                }
            }
            num += temp;
            print("w: " + w + " has " + temp + " vowels");
        }
        System.out.println(words);
        System.out.println(num);

    }
}
