package holding;

import java.util.*;
import static util.Common.*;

public class PrintingContainers {
    static Collection fill(Collection<String> collection) {
        collection.add("rat");
        collection.add("cat");
        collection.add("dog");
        collection.add("dog");
        return collection;
    }
    static Map fill(Map<String,String> map) {
        map.put("rat", "Fuzzy");
        map.put("cat", "Rags");
        map.put("dog", "Bosco");
        map.put("dog", "Spot");
        return map;
    }
    public static void main(String[] args) {
        print(fill(new ArrayList<String>()));
        print(fill(new LinkedList<String>()));//双向链表
        print(fill(new HashSet<String>()));//集合（唯一，查询快，插入慢）
        print(fill(new TreeSet<String>()));//有序结合（唯一，有序）

        print(fill(new LinkedHashSet<String>()));//LinkedHashSet将会以元素的添加顺序访问集合的元素。
        HashMap<String, String> map = new HashMap<String,String>();
        print(fill(map));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            System.out.println("Key: " + e.getKey() + "--Value: "
                    + e.getValue());
        }
        print(fill(new TreeMap<String,String>()));
        print(fill(new LinkedHashMap<String,String>()));//有序，key不重复 （HashMap + LinkedList）


    }
}