package holding;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Statistics {
    public static void main(String[] args) {
        Random random = new Random(47);
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < 10000; i ++) {
            Integer integer = random.nextInt(20);
            Integer val = map.get(integer);
            if (val != null) {
                map.put(integer, val + 1);
            } else {
                map.put(integer, 1);
            }
        }
        System.out.println(map);
    }
}
