package holding;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class ForeachCollections {
    public static void main(String[] args) {
        Collection<String> collection = new LinkedList<String>();
        Collections.addAll(collection, "take the long way home".split(" "));
        for (String s : collection) {
            System.out.print("'" + s + "'");
        }
    }
}
