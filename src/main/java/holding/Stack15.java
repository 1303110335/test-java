package holding;

import net.mindview.util.Stack;

public class Stack15 {

    private static Stack<Character> stack = new Stack<Character>();

    public static void main(String[] args) {

        String s = "+U+n+c---+e+r+t---+a-+i-+n+t+y---+ -+r+u--+l+e+s---";
        evaluate(s);
        System.out.println(stack);
    }

    private static void evaluate(String expr) {
        char[] data = expr.toCharArray();
        for (int i=0; i< data.length; i++) {
            switch (data[i]) {
                case '+':
                    stack.push(data[++i]);
                    break;
                case '-':
                    System.out.println(stack.pop());
                    break;
            }
//            System.out.println(stack);
        }
    }

}
