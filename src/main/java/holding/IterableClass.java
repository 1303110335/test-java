package holding;

import java.util.Iterator;

public class IterableClass implements Iterable<String> {
    protected String[] words = ("and that is how we know the earth to be banana-shaped.").split(" ");

    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private int index = 0;
            public void remove() {
                throw new UnsupportedOperationException();
            }

            public boolean hasNext() {
                return index < words.length;
            }

            public String next() {
                return words[index++];
            }
        };
    }

    public static void main(String[] args) {
        Iterable<String> iterableClass = new IterableClass();
        for (String s : iterableClass)
            System.out.println(s);
    }
}
