package io;

import net.mindview.util.Directory;
import net.mindview.util.PPrint;

import java.io.File;

import static util.Common.print;

public class DirectoryDemo {
    public static void main(String[] args) {
        //All directories
//        PPrint.pprint(Directory.walk(".").dirs);
//        for (File file : Directory.local(".", "first.*"))
//            print(file);
//        print("--------------------");
//        for (File file : Directory.walk("src/main/java", "T.*\\.java"))
//            print(file);

        print("=======================");
        for (File file : Directory.walk("src/main/java", ".*[Zz].*\\.class"))
            print(file);
    }
}
