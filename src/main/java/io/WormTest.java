package io;

import java.io.*;

import static util.Common.print;

public class WormTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Worm wm = new Worm(3, 'x');
        print(wm);

        ObjectOutputStream outputStream = new ObjectOutputStream(
                new FileOutputStream("wormTest.txt")
        );

        outputStream.writeObject(wm);
        outputStream.close();

        ObjectInputStream inputStream = new ObjectInputStream(
                new FileInputStream("wormTest.txt")
        );
        Worm w = (Worm) inputStream.readObject();
        print(w);
    }
}
