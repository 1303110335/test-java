package io;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

import static net.mindview.util.Print.print;

public class RandomNumbers {
    //有1千万个随机数，随机数的范围在1到1亿之间。现在要求写出一种算法，将1到1亿之间没有在随机数中的数求出来？
    static List<Integer> lists = new ArrayList<>();
    static BitSet outOfbitSet = new BitSet(10000000);
    static Random rand = new Random();

    public static void main(String[] args) {


        for (int i = 0; i < 10000000; i++) {
            Integer integer = rand.nextInt(100000000);
            lists.add(integer);
        }

        for (int i = 0; i < 100000000; i ++) {
            if (lists.get(i) != null) {
//                print(lists.get(i));
                outOfbitSet.set(lists.get(i));
            }
        }

    }
}
