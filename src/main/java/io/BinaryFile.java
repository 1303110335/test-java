package io;

import java.io.*;
import java.util.*;

import static util.Common.print;

public class BinaryFile {
    public static byte[] read(File bFile) throws IOException {
        BufferedInputStream bf = new BufferedInputStream(
                new FileInputStream(bFile)
        );
        try {
            byte[] data = new byte[bf.available()];
            bf.read(data);
            return data;
        } finally {
            bf.close();
        }
    }

    public static byte[] read(String bFile) throws IOException {
        return read(new File(bFile).getAbsoluteFile());
    }

    public static void main(String[] args) throws IOException {
        byte[] bytes = BinaryFile.read("src/main/java/io/BinaryFile.java");
        Map<Byte, Integer> map = new HashMap<>();
        for (byte b : bytes) {
            if (map.containsKey(b)) {
                map.put(b, map.get(b) + 1);
            } else {
                map.put(b, 1);
            }
        }
        List<Byte> keys = new ArrayList<>(map.keySet());
        Collections.sort(keys);
        for (byte key : keys)
            System.out.println((char)key + " => " + map.get(key));
    }
}
