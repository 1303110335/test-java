package io;

import net.mindview.util.PPrint;
import sun.reflect.generics.tree.Tree;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static util.Common.print;

public final class Directory {
    //TreeInfo  class
    public static class TreeInfo implements Iterable<File> {
        //dirs
        public List<File> dirs = new ArrayList<File>();
        //files
        public List<File> files = new ArrayList<File>();
        @Override
        public Iterator iterator() {
            return files.iterator();
        }

        public void addAll(TreeInfo other) {
            this.dirs.addAll(other.dirs);
            this.files.addAll(other.files);
        }

        @Override
        public String toString() {
            return "dirs: " + PPrint.pformat(dirs) +
                    "\n\nfiles: " + PPrint.pformat(files);
        }
    }

    public static TreeInfo walk(String start, String regex) {
        return recurseDirs(new File(start), regex);
    }

    public static TreeInfo walk(String start) {
        return recurseDirs(new File(start), "*");
    }

    //recurseDirs func
    static TreeInfo recurseDirs(File files, String regex) {
        TreeInfo result = new TreeInfo();
        if (files.isDirectory()) {
            for (File f : files.listFiles()) {
                result.dirs.add(f);
                result.addAll(recurseDirs(f, regex));
            }
        } else {
            result.files.add(files);
        }
        return result;
    }

    public static void main(String[] args) {
        TreeInfo treeInfo = walk(".");
        print(treeInfo);
    }

}
