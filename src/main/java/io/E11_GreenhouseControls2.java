package io;

import innerclasses.GreenhouseControls;
import innerclasses.controller.Event;

public class E11_GreenhouseControls2 extends GreenhouseControls {
    class Restart extends Event {
        private Event[] eventList;
        public Restart(long delayTime) {
            super(delayTime);
        }

        @Override
        public void action() {
            for(Event e : eventList) {
                e.start();
                addEvent(e);
            }
            start();
            addEvent(this);//Return this Event
        }

        public String toString() {
            return "Restarting system";
        }

        public void setEventList(Event[] eventList) {
            this.eventList = eventList;
        }
    }

}
