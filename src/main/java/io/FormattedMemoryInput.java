package io;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class FormattedMemoryInput {
    public static void main(String[] args) {
        try {
            DataInputStream in = new DataInputStream(
                    new ByteArrayInputStream(
                            BufferedInputFile.read("src/main/java/io/FormattedMemoryInput.java").getBytes()
                    )
            );
//            while (true)
//                System.out.print((char)in.readByte());

            while (in.available() != 0)
                System.out.print((char)in.readByte());
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("End of stream");
        }
    }
}
