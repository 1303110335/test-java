package io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import static util.Common.print;

public class BufferedInputFile {
    public static String read(String filename) throws IOException {
        //Reading input by lines
        BufferedReader in = new BufferedReader(new FileReader(filename));
        String s;
        StringBuilder sb = new StringBuilder();
//        List<String> stringList = new LinkedList<>();
        while((s = in.readLine()) != null){
            sb.append(s + "\n");
        }
        in.close();
        return sb.toString();
//        Iterator iterator = stringList.listIterator(stringList.size());
//
//        while (((ListIterator) iterator).hasPrevious()) {
//            print(((ListIterator) iterator).previous());
//        }

    }

    public static void main(String[] args) throws IOException {
        read("src/main/java/io/BufferedInputFile.java"/*, "java"*/);
    }
}
