package io.xml;


import io.xml.inter.Task;
import io.xml.tool.DiffTime;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static util.Common.print;

public class JDOMReader {
    public static void main(String[] args) {
        DiffTime df = new DiffTime();
        df.diffTime(new Task() {
            @Override
            public void run() {
                try {
                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(new File("person.xml"));
                    Element foo = doc.getRootElement();
                    List allChildren = foo.getChildren();
                    for (int i = 0; i < allChildren.size(); i++) {
                        print("first: " + ((Element)allChildren.get(i)).getChild("first").getText());
                        print("last: " + ((Element)allChildren.get(i)).getChild("last").getText());
                    }
                } catch (JDOMException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
