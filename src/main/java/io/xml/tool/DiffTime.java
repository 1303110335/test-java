package io.xml.tool;

import io.xml.inter.Task;
import io.xml.inter.Time;

public class DiffTime implements Time {

    @Override
    public void diffTime(Task task) {
        long lasting = System.currentTimeMillis();
        task.run();
        System.out.println("runtime:" + (System.currentTimeMillis() - lasting) + "毫秒");
    }
}
