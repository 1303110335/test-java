package io.xml;

import io.xml.inter.Task;
import io.xml.tool.DiffTime;
import io.xml.tool.Person;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MYSAXReader extends DefaultHandler {
    Stack tags = new Stack();

    private List<Person> persons;
    private Person person;
    private String tag;

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        persons = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        tag = qName;
        if ("person".equals(qName)) {
            person = new Person();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        tag = "";
        if ("person".equals(qName))
            persons.add(person);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        String string = new String(ch, start, length);
        if ("first".equals(tag))
            person.setFirst(string);
        else if ("last".equals(tag))
            person.setLast(string);
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public static void main(String[] args) {

        DiffTime df = new DiffTime();
        df.diffTime(new Task() {
            @Override
            public void run() {
                try {
                    //创建对象
                    SAXParserFactory sf = SAXParserFactory.newInstance();
                    //获取解析器
                    SAXParser sp = sf.newSAXParser();
                    MYSAXReader reader = new MYSAXReader();
                    sp.parse(new InputSource("person.xml"), reader);
                    List<Person> personList = reader.getPersons();
                    System.out.println(reader.getPersons());
                } catch (SAXException e) {
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
