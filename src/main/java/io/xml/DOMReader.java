package io.xml;


import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DOMReader {

    public static void main(String args[]) {
        long lasting = System.currentTimeMillis();
        try {
            File f = new File("person.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(f);
            NodeList nl = doc.getElementsByTagName("person");
            for (int i = 0; i < nl.getLength(); i++) {
                System.out.println("first:" + doc.getElementsByTagName("first").item(i)
                .getFirstChild().getNodeValue());
                System.out.println("last:" + doc.getElementsByTagName("last").item(i)
                        .getFirstChild().getNodeValue());
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("runtime:" + (System.currentTimeMillis() - lasting) + "毫秒");
    }

    // Produce an XML Element from this Person object:

}
