package io.xml;

import io.xml.inter.Task;
import io.xml.tool.DiffTime;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Iterator;

import static util.Common.print;

public class DOM4JReader {
    public static void main(String[] args) {
        DiffTime df = new DiffTime();
        df.diffTime(new Task() {
            @Override
            public void run() {
                try {
                    File f = new File("person.xml");
                    SAXReader reader = new SAXReader();
                    Document doc = reader.read(f);
                    Element root = doc.getRootElement();
                    Element foo;
                    for (Iterator i = root.elementIterator("person"); i.hasNext();) {
                        foo = (Element) i.next();
                        print("first: " + foo.elementText("first"));
                        print("last: " + foo.elementText("last"));
                    }
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
