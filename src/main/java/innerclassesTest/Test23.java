package innerclassesTest;

import static util.Common.*;

interface U {
    String x();
    String y();
    String z();
}

class A {
    private static int i = 0;
    private int count = i++;
    public U getU() {
        return new U() {
            public String x() {
                print("x" + count);
                return "x";
            }

            public String y() {
                print("y" + count);
                return "y";
            }

            public String z() {
                print("z" + count);
                return "z";
            }
        };
    }
}

class B {
    //U组成的数组
    U[] ua;

    public B (int size) {
        ua = new U[size];
    }

    public boolean add (U u) {
        for (int i =0; i < ua.length; i++) {
            if (ua[i] == null) {
                ua[i]= u;
                return true;
            }
        }
        return false;//could't find any place
    }

    public boolean setNull(int i) {
        if (i < 0 || i >= ua.length) {
            return false;
        }

        ua[i] = null;
        return true;
    }

    public void traverse() {
        for (U temp: ua) {
            if (temp != null) {
                temp.x();
                temp.y();
                temp.z();
            }
        }
    }

}

public class Test23 {
    public static void main(String[] args) {
        B b1 = new B(10);
        for(int i = 0; i<10; i++) {
            b1.add((new A()).getU());
        }
        b1.traverse();
        b1.setNull(5);
        b1.setNull(8);
        b1.setNull(2);
        b1.setNull(4);
        b1.traverse();
    }
}
