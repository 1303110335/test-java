package innerclassesTest.controller;

public class GreenhouseControllerJet {

    public static void main(String[] args) {
        GreenhouseControlsJet gj = new GreenhouseControlsJet();
        //设置时钟
        gj.addEvent(gj.new Bell(600));
        //eventList
        Event[] events = {
                gj.new ThermostatNight(200),
                gj.new LightOn(400),
                gj.new LightOff(600),
                gj.new WaterOn(800),
                gj.new WaterOff(1000),
                gj.new FanOpen(1200),
                gj.new FanClose(1400),
                gj.new WaterJetOn(1600),
                gj.new WaterJetOff(1800),
                gj.new ThermostatDay(2000)
        };
        gj.addEvent(gj.new Restart(2500, events));
        gj.addEvent(new GreenhouseControls.Terminate(new Integer(3000000)));
        gj.run();
    }
}
