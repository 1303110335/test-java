package innerclassesTest.controller;

public class GreenhouseControlsJet extends GreenhouseControls {

    private boolean generator = true;
    public class WaterJetOn extends Event {
        public WaterJetOn(long delayTime) {
            super(delayTime);
        }

        public void action() {
            generator = true;
        }

        public String toString() {
            return "waterJet is on";
        }
    }

    public class WaterJetOff extends Event {
        public WaterJetOff(long delayTime) {
            super(delayTime);
        }

        public void action() {
            generator = false;
        }

        public String toString() {
            return "waterJet is off";
        }
    }

}
