package innerclassesTest;

class Outer {
    class Inner {
        {System.out.println("Inner class created");}
    }

    public Inner getInner() {
        return new Inner();
    }
}

public class ReferenceToInnerClass {
    public static void main(String[] args) {
        Outer outer = new Outer();
        Outer.Inner inner = outer.getInner();
    }
}
