package innerclassesTest;

public interface Destination {
    String readLabel();
}
