package containers;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static net.mindview.util.Countries.*;

public class Acountries {
    public static void main(String[] args) {
        TreeMap<String, String> map = new TreeMap<String, String>(capitals());
        TreeSet<String> set = new TreeSet<String>(names());

        String b = null;
        for (String s : map.keySet()) {
            if (s.startsWith("A")) {
                b = s;
                break;
            }
        }
        Map<String, String> aMap = map.headMap(b);
        Set<String> aSet = set.headSet(b);
    }
}
