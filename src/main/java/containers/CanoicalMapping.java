package containers;

import java.util.WeakHashMap;

class Element {
    private String ident;
    public Element(String id ) { ident = id; }
    public String toString() { return ident; }
    public int hashCode() { return ident.hashCode(); }

    @Override
    public boolean equals(Object r) {
        return r instanceof Element && ident.equals(((Element) r).ident);
    }

    //子类可以覆盖该方法以实现资源清理工作，GC在回收对象之前调用该方法。
    protected void finalize() {
        System.out.println("Finalizing " + getClass().getSimpleName() + " " + ident);
    }
}

class Key extends Element {

    public Key(String id) {
        super(id);
    }
}

class Value extends Element {

    public Value(String id) {
        super(id);
    }
}

public class CanoicalMapping {
    public static void main(String[] args) {
        int size = 1000;
        Key[] keys = new Key[size];
        WeakHashMap<Key, Value> map = new WeakHashMap<>();
        for (int i = 0; i < size; i++) {
            Key k = new Key(Integer.toString(i));
            Value v = new Value(Integer.toString(i));
            if (i % 3 == 0)
                keys[i] = k;
            map.put(k, v);
        }
        System.gc();
    }
}
