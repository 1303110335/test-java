package containers;

import net.mindview.util.*;

import java.util.Iterator;

import static util.Common.print;

class Letters implements Generator<Pair<Integer, String>>, Iterable<Integer> {

    private int size = 9;
    private int number = 1;
    private char letter = 'A';

    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            public void remove() {
                throw new UnsupportedOperationException();
            }

            public boolean hasNext() {
                return number < size;
            }

            public Integer next() {
                return number++;
            }
        };
    }

    public Pair<Integer, String> next() {
        return new Pair<Integer, String>(number++, "" + letter++);
    }
}

public class MapDataTest {
    public static void main(String[] args) {
        //Pair Generator
        print(MapData.map(new Letters(), 11));
        print(MapData.map(new CountingGenerator.Character(), new RandomGenerator.String(3), 8));
        print(MapData.map(new CountingGenerator.Character(), "Value", 6));
        print(MapData.map(new Letters(), new RandomGenerator.String(3)));
        print(MapData.map(new Letters(), "Pop"));
    }
}
