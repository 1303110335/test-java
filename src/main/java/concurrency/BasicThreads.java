package concurrency;

import static util.Common.print;

public class BasicThreads {
    public static void main(String[] args) {
        Thread t = new Thread(new LiftOff());
        t.start();
        print("Waiting for LiftOff");
    }
}
