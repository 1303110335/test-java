package concurrency;

import static java.lang.Thread.yield;
import static util.Common.print;

class Printer implements Runnable {
    private static int num = 0;
    private final int id = num++;
    @Override
    public void run() {
        print("Stage 1..., ID = " + id);
        Thread.yield();
        print("Stage 2..., ID = " + id);
        Thread.yield();
        print("Stage 3..., ID = " + id);
        Thread.yield();
        print("Printer ended, ID = " + id);
    }
}

public class Runable_01 {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++)
            new Thread(new Printer()).start();
    }
}


