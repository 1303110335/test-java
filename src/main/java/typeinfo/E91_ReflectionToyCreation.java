package typeinfo;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;



class Toy {
    // Comment out the following default constructor
    // to see NoSuchMethodError from (*1*)
    Toy() {
    }

    Toy(int i) {
    }
}

interface HasBatteries {
}

interface Waterproof {
}

interface Shoots {
}

class FancyToy extends Toy implements HasBatteries, Waterproof, Shoots {
    FancyToy() {
        super(1);
    }
}

class SuperToy extends FancyToy {
    int IQ;
    public SuperToy(int intelligence) {
        IQ = intelligence;
    }
    public String toString() {
        return "I'm a SuperToy. I'm smarter than you.";
    }
}

public class E91_ReflectionToyCreation {
    public static Toy makeToy(String toyName, int IQ) {
        try {
            Class<?> tClass = Class.forName(toyName);
            for (Constructor<?> ctor : tClass.getConstructors()) {
                //Look for a constructor whit a single parameter:
                Class<?>[] params = ctor.getParameterTypes();
                if (params.length == 1){
                    if (params[0] == int.class) {
                        return (Toy)ctor.newInstance(new Object[]{Integer.valueOf(IQ)});
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(makeToy("typeinfo.SuperToy", 150));
    }
}
