package typeinfo;//: typeinfo/GenericClassReferences.java

import static util.Common.print;

public class GenericClassReferences {
    public static void main(String[] args) {
        Class intClass = int.class;
        Class<Integer> genericIntClass = int.class;
        genericIntClass = Integer.class; // Same thing
        intClass = double.class;
//         genericIntClass = double.class; // Illegal

        Class<?> int2Class = int.class;
        int2Class = double.class;
    }
} ///:~
