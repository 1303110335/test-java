package typeinfo;

import static util.Common.print;

public class TestClass {
    public static void main(String[] args) throws ClassNotFoundException {
        String className = "typeinfo.pets.Mouse";
        classInfo(Class.forName(className));

    }

    public static void classInfo(Class<?> c) {
        print(c.getClass());
        print(c.getPackage());
        print(c.getSuperclass());
        print(c.getDeclaredClasses());
        print(c.getClasses());
        print(c.getInterfaces());
        print(c.getDeclaredMethods());
        print(c.getMethods());
        print(c.getDeclaredConstructors());

    }
}
