package enumerated;

import net.mindview.util.Generator;

import java.util.Random;

enum CartoonCharacters  {
    SLAPPY, SPANKY, PUNCHY, SILLY, BOUNCY, NUTTY, BOB;

    private static Random rand = new Random(47);

    public static CartoonCharacters next() {
        return values()[rand.nextInt(values().length)];
    }
}

public class EnumImplementation2 {

    public static <T> void printNext() {
        System.out.println(CartoonCharacters.next() + ".");
    }

    public static void main(String[] args) {
        // Choose any insstance:
        for (int i = 0; i < 10; i++)
            printNext();
    }
}
