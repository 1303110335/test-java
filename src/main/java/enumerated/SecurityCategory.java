package enumerated;

public enum  SecurityCategory {
    SOTCK(Security.Stock.class), BOND(Security.Boud.class);
    Security[] values;
    SecurityCategory (Class<? extends Security> kind) {
        values = kind.getEnumConstants();
    }

    public Security randomSelection() {
        return Enums.random(values);
    }

    interface Security {
        enum Stock implements Security { SHORT, LONG, MARGIN }
        enum Boud implements Security { MUNICIPAL, JUNK }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            SecurityCategory category =
                    Enums.random(SecurityCategory.class);
            System.out.println(category + ": " + category.randomSelection());
        }
    }
}
