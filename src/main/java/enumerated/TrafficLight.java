package enumerated;

import static util.Common.print;

// Define an enum type:
enum Signal { Green, Yellow, Red, }

public class TrafficLight {
    Signal color = Signal.Red;
    public void change() {
        switch (color) {
            // Note that you don't have to say Signal.Red
            //in the case statement:
            case Red:
                color = Signal.Green;
                break;
            case Green:
                color = Signal.Yellow;
                break;
            case Yellow:
                color = Signal.Red;
                break;
        }
    }

    public String toString() {
        return "The traffic light is " + color;
    }

    public static void main(String[] args) {
        TrafficLight t = new TrafficLight();
        for (int i = 0; i < 7; i++) {
            print(t);
            t.change();
        }
    }
}
