package generics.coffee;

import net.mindview.util.Generator;
import net.mindview.util.TypeCounter;

import java.util.Iterator;
import java.util.Random;

import static util.Common.print;
import static util.Common.printnb;

public class CoffeeGenerator implements Generator<Coffee>, Iterable<Coffee> {

    private Class[] types = {
            Latte.class, Mocha.class, Cappuccino.class, Americano.class, Breve.class
    };

    private static Random rand = new Random(47);

    public CoffeeGenerator() {};

    private int size = 0;

    public CoffeeGenerator(int sz) {
        size = sz;
    }

    public Iterator<Coffee> iterator() {
        return new CoffeeIterator();
    }

    public Coffee next() {
        try {
            return (Coffee)types[rand.nextInt(types.length)].newInstance();
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    private class CoffeeIterator implements Iterator<Coffee> {
        int count = size;
        public boolean hasNext() {
            return count > 0;
        }

        public Coffee next() {
            count --;
            return CoffeeGenerator.this.next();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public static void main(String[] args) {
        /*CoffeeGenerator gen = new CoffeeGenerator();
        for (int i = 0; i < 5; i++) {
            System.out.println(gen.next());
        }
        for (Coffee c : new CoffeeGenerator(3)) {
            System.out.println(c);
        }*/

        TypeCounter counter = new TypeCounter(Coffee.class);
        for (Coffee coffee : new CoffeeGenerator(10)) {
            printnb(coffee.getClass().getSimpleName() + " ");
            counter.count(coffee);
        }
        print();
        print(counter);
    }
}
