package generics;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class Sets2 {

    @SuppressWarnings("unchecked")
    public static <T> Set<T> copy(Set<T> s) {
        if (s instanceof EnumSet)
            return ((EnumSet) s).clone();
        return new HashSet<T>(s);
    }

    public static <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> result = copy(a);
        result.addAll(b);
        return result;
    }
}
