package generics;

public class Holder<T> {
    private T value;

    public Holder() {}

    public Holder(T val) {
        this.value = val;
    }

    public T get() {
        return value;
    }

    public void set(T value) {
        this.value = value;
    }

    public boolean equals(Object obj) {
        return value.equals(obj);
    }

    public static void main(String[] args) {
        Holder<Apple> appleHolder = new Holder<Apple>(new Apple());
        Apple d = appleHolder.get();
        appleHolder.set(d);

//        Holder<Fruit> fruitHolder = appleHolder;
        Holder<? extends Fruit> fruit = appleHolder;
        Fruit p = fruit.get();
        d = (Apple) fruit.get();
        System.out.println(d);
        try {
            Orange c = (Orange)fruit.get();
        } catch (Exception e) {
//            fruit.set(new Apple());
            System.out.println(e);
        }
        System.out.println(fruit.equals(d));
    }
}
