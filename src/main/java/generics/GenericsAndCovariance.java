package generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericsAndCovariance {
    public static void main(String[] args) {
        List<? extends Fruit> fruits = new ArrayList<Apple>();
//        fruits.add(new Apple());
//        fruits.add(new Fruit());
//        fruits.add(new Object());

        fruits.add(null);
        Fruit fruit = fruits.get(0);


//        Number[] num = new Integer[10];
//        num[0] = Integer.valueOf(1);

        List<? extends Number> nlist = new ArrayList<Integer>();
//        nlist.add(new Integer(1));
        nlist.add(null);
        Number n = nlist.get(0);

        List<? extends Fruit> flist = Arrays.asList(new Apple());
        Boolean res = flist.contains(new Apple());
        System.out.println(res);
//        flist.add(new Apple());

    }
}
