package generics;

import net.mindview.util.Generator;

import java.util.ArrayList;
import java.util.Random;

class Container {
    private final int id;
    private String desc;
    private double price;

    public Container (int id, String desc, Double price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    @Override
    public String toString() {
        return "id: " + id + " desc: " + desc + " pirce: " + price;
    }

    public static Generator<Container> generator =
            new Generator<Container>() {
                private Random rand = new Random(47);
                public Container next() {
                    return new Container(rand.nextInt(1000),
                            "text", Math.round(rand.nextDouble() * 1000) + 0.99);
                }
            };
}

class Shelfs extends ArrayList<Container> {
    public Shelfs(int nContainers) {
        Generators.fill(this, Container.generator, nContainers);
    }
}

class Ship extends ArrayList<Shelfs> {
    public Ship (int nShelfs, int nContainers) {
        for (int i = 0; i < nShelfs; i++)
            add(new Shelfs(nContainers));
    }
}

public class ShipStore extends ArrayList<Ship> {
    public ShipStore(int nShips, int nShelfs, int nContainers) {
        for (int i = 0; i < nShips; i++)
            add(new Ship(nShelfs, nContainers));
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Ship ship : this)
            for (Shelfs shelfs : ship)
                for (Container c : shelfs) {
                    stringBuilder.append(c);
                    stringBuilder.append("\n");
                }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        System.out.println(new ShipStore(10, 5, 10));
    }
}
