package generics;

interface Payable<T> {}

class Employees implements Payable<Employee> {}

//不能两次实现相同的接口（擦除）
//class Hourly extends Employees implements Payable<Hourly> {} ///:~

