package generics;

import java.util.HashMap;
import java.util.Map;

import static util.Common.print;

class Building{}

class House extends Building{}

public class ClassTypeCapture<T> {
    private Map<String, Class<?>> types = new HashMap<String, Class<?>>();

    public void addType(String typename, Class<?> kind) {
        types.put(typename, kind);
    }

    public Object createNew(String typename)
    {
        Class<?> cl = types.get(typename);
        try {
            return cl.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*Class<T> kind;
    public ClassTypeCapture(Class<T> kind) {
        this.kind = kind;
    }
    public boolean f(Object arg) {
        return kind.isInstance(arg);
    }*/

    public static void main(String[] args) {
        /*ClassTypeCapture<Building> ctt1 = new ClassTypeCapture<Building>(Building.class);
        System.out.println(ctt1.f(new Building()));
        System.out.println(ctt1.f(new House()));
        ClassTypeCapture<House> ctt2 = new ClassTypeCapture<House>(House.class);
        System.out.println(ctt2.f(new Building()));
        System.out.println(ctt2.f(new House()));*/

        ClassTypeCapture ctt = new ClassTypeCapture();
        ctt.addType("Building", Building.class);
        ctt.addType("House", House.class);
        ctt.addType("Product", Product.class);
        print(ctt.createNew("Building").getClass());
        print(ctt.createNew("House").getClass());
//        ctt.createNew("Product");//Product does not have a no-arg constructor
        ctt.createNew("Car");//Does not have a Car type
    }
}
