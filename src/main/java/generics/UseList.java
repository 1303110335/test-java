package generics;

import java.util.List;

public class UseList<W,T> {
//    void f(List<T> v) {}//both methods have same erasure
    void f(List<W> v) {}
    void f2(List<T> v) {}
}
