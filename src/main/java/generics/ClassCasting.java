package generics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class ClassCasting {
    public void f(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(args[0]));
//        List<Widget> lw1 = List<Widget>.class.cast(in.readObject());
        List<Widget> shapes = (List<Widget>) in.readObject();
        //通过泛型来转型
        List<Widget> lw2 = List.class.cast(in.readObject());
    }
}
