package exceptions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class InputFile {
    private BufferedReader in;
    public InputFile(String fname) throws Exception {
        try {
            in = new BufferedReader(new FileReader(fname));
        } catch (FileNotFoundException e) {
            System.out.println("Could not open " + fname);
            throw e;
        } catch (Exception e) {
            try {
                in.close();
            } catch (IOException e2) {
                System.out.println("in.close() unsuccessful");
            }
            throw e;
        } finally {
            //Don't close it here
        }
    }

    public String getLine() {
        String s;
        try {
            s = in.readLine();
        } catch (IOException e) {
//            e.printStackTrace();
            throw new RuntimeException("readLine() failed");
        }
        return s;
    }

    public void dispose() {
        try {
            in.close();
            System.out.println("dispose() successful");
        } catch (IOException e) {
//            e.printStackTrace();
            throw new RuntimeException("in.close() failed");
        }
    }

    public static void main(String[] args) {
        try {
            InputFile inputFile = new InputFile("E:\\java\\testJava\\src\\main\\java\\exceptions\\InputFile.java");
            try {
                String s;
                int i = 1;
                while((s = inputFile.getLine()) != null) {
//                    System.out.println(s);
                }
            } catch (Exception e) {
                System.out.println("Caught Exception in main");
                e.printStackTrace(System.out);
            } finally {
                inputFile.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("InputFile construction failed");
        }
    }
}
