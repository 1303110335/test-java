package exceptions;

class VeryImportantException extends Exception {
    public String toString() {
        return "A very import exception!";
    }
}

class HohumException extends Exception {
    public String toString() {
        return "A trivial exception";
    }
}

public class LostException {
    void f() throws VeryImportantException {
        throw new VeryImportantException();
    }

    void dispose() throws HohumException {
        throw new HohumException();
    }

    public static void main(String[] args) {
        try {
            LostException ln = new LostException();
            try {
                ln.f();
            } catch (VeryImportantException e) {
                e.printStackTrace();
            } finally {
                ln.dispose();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
