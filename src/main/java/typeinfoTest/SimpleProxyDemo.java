package typeinfoTest;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;

import static util.Common.print;

interface Interface {
    void doSomething();

    void somethingElse(String arg);
}

class RealObject implements Interface {

    public void doSomething() {
        print("doSomething");
    }

    public void somethingElse(String arg) {
        print("somethingElse " + arg);
    }
}

class MethodCounter implements InvocationHandler {

    private Object proxied;
    public static HashMap<String, Integer> map = new HashMap<String, Integer>();

    public MethodCounter(Object proxied) {
        this.proxied = proxied;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (map.get(method.getName()) != null)
            map.put(method.getName(), map.get(method.getName()) + 1);
        else
            map.put(method.getName(), 1);
        return method.invoke(proxied, args);
    }
}

class SimpleProxy implements Interface {

    private Interface proxied;

    public SimpleProxy(Interface proxied) {
        this.proxied = proxied;
    }

    public void doSomething() {
        print("SimpleProxy doSomething");
        proxied.doSomething();
    }

    public void somethingElse(String arg) {
        print("SimpleProxy somethingElse " + arg);
        proxied.somethingElse(arg);
    }
}


public class SimpleProxyDemo {
    public static void consumer(Interface iface) {
        iface.doSomething();
        iface.somethingElse("bonobo");
        iface.somethingElse("bonobo");
    }

    public static void main(String[] args) {
//        consumer(new RealObject());
//        consumer(new SimpleProxy(new RealObject()));

        Interface proxy = (Interface) Proxy.newProxyInstance(
                Interface.class.getClassLoader(),
                new Class[]{Interface.class},
                new MethodCounter(new SimpleProxy(new RealObject()))
        );
        consumer(proxy);
        print(MethodCounter.map);
    }
}


