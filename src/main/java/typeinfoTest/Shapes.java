package typeinfoTest;

import java.util.Arrays;
import java.util.List;

import static util.Common.print;

abstract class Shape {
    void draw() {
        System.out.println(this + ".draw()");
    }
    abstract public String toString();
}


class Circle extends Shape {

    public String toString() {
        return "Circle";
    }
}

class Square extends Shape {

    public String toString() {
        return "Square";
    }
}

class Triangle extends Shape {

    public String toString() {
        return "Triangle";
    }
}

public class Shapes{
    public static void main(String[] args) {
        List<Shape> shapeList = Arrays.asList(
                new Circle(), new Square(), new Triangle()
        );
        for (Shape s : shapeList) {
            s.draw();
        }

        Shape shape = new Square();
        Square c=  null;
        if (shape instanceof Shape) {
            c = (Square)shape;
        }
        System.out.println(c);

        char[] ac = "Hello World".toCharArray();
        print(ac.getClass());
        print(ac.getClass().getSuperclass());
    }
}