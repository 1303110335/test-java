package typeinfoTest.factory;

public interface Factory<T> {
    T create();
}
