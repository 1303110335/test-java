package net.mindview.util;

import typeinfo.pets.Cat;
import typeinfo.pets.Pet;

public class SixTuple<A, B, C, D, E, F> extends FiveTuple<A,B,C,D,E> {

    public final F sixth;
    public SixTuple(A a, B b, C c, D d, E e, F f) {
        super(a, b, c, d, e);
        sixth = f;
    }

    @Override
    public String toString() {
        return "SixTuple{" +
                "sixth=" + sixth +
                ", fifth=" + fifth +
                ", fourth=" + fourth +
                ", third=" + third +
                ", first=" + first +
                ", second=" + second +
                '}';
    }

    public static void main(String[] args) {
        System.out.println(new SixTuple<String, Integer, Cat, Double, Long, String>(
                "haha", 1, new Cat(), 1.24, 1l, "123"
        ));
    }
}
