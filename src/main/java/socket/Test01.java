package socket;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import static util.Common.print;

public class Test01 {
    public static void main(String[] args) throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();
        print("ip：" + address.getHostAddress());
        print("计算名：" + address.getHostName());

        byte[] bytes = address.getAddress();
        print("字节数组形式的ip:" + Arrays.toString(bytes));
        print(address);

        
    }
}
