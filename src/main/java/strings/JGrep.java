package strings;

import net.mindview.util.TextFile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JGrep {

    static final String CMNT_EXT_REGEX =
            "(?x)(?m)   # Comments, Multiline & Dotall(?s) : ON\n" +
            "/\\*           # Match START OF THE COMMENT\n" +
            "(.*?)          # Match all chars\n" +
            "\\*/           # Match END OF THE COMMENTS\n" +
            "| //(.*)?$     # OR Match C++ style comments\n";

    static final String CLASS_REGEX = "class\\s+(\\w+)";

    public static void main(String[] args) {
        String s = TextFile.read("src\\main\\java\\strings\\JGrep.java");
        Matcher m = Pattern.compile(CLASS_REGEX).matcher(s);
        while (m.find()) {
            System.out.println(m.group());
        }


        //123
        /*123*/
    }
}
