package strings;
import static net.mindview.util.Print.*;
public class IntegerMatch {
    public static void main(String[] args) {
        System.out.println("-1234".matches("-?\\d+"));
        System.out.println("5678".matches("-?\\d+"));
        System.out.println("+911".matches("-?\\d+"));
        System.out.println("+911".matches("(-|\\+)?\\d+"));

        print("a found a tree".replaceFirst("f\\w+", "located"));
        print("a found a tree".replaceAll("shrubbery|tree", "banana"));
    }


}
