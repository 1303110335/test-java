package strings;

import net.mindview.util.TextFile;

//import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static util.Common.print;

public class TheReplacement {
    static final int bufferSize = 1024;
    static final char[] buffer = new char[bufferSize];
    public static void main(String[] args) throws Exception {
        String s = TextFile.read("src\\main\\java\\strings\\TheReplacement.java");
        Matcher mInput = Pattern.compile("/\\*!(.*)!\\*/", Pattern.DOTALL).matcher(s);
        if (mInput.find()) {
            s = mInput.group(1);
        }
        s = s.replaceAll(" {2,}", "");
        s = s.replaceAll("(?m)^ +", "");
        print(s);
        s = s.replaceFirst("[aeiou]", "(VOWEL1)");
        print(s);
        StringBuffer sbuf = new StringBuffer();
        Pattern p = Pattern.compile("[aeiou]");
        Matcher m = p.matcher(s);
        while (m.find())
            m.appendReplacement(sbuf, m.group().toUpperCase());
        m.appendTail(sbuf);
        print(sbuf);
    }
}
