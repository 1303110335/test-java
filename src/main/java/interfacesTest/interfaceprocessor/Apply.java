package interfacesTest.interfaceprocessor;


import util.Common;

public class Apply {
    public static void process(Processor p, Object s) {
        Common.print("Using Proccessor " + p.name());
        Common.print(p.process(s));
    }
}
