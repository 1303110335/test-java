package interfacesTest.interfaceprocessor;

public interface Processor {
    String name();
    Object process(Object input);
}
