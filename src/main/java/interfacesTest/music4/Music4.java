package interfacesTest.music4;

import polymorphism.music.Note;
import util.Common;

abstract class Instrument {
    private int i;
    public abstract void play(Note n);
    public String what() {
        return "Instrument";
    }
    public abstract void adjust();
}

class Wind extends Instrument {
    public void play(Note n) {
        Common.print("Wind.play() " + n);
    }

    public String what() {
        return "Wind";
    }

    public void adjust() {
        Common.print("Wind adjust()");
    }
}

class Percussion extends Instrument {
    public void play(Note n) {
        Common.print("Percussion.play() " + n);
    }

    public String what() {
        return "Percussion";
    }

    public void adjust() {

    }
}

class Stringed extends Instrument {
    public void play(Note n) {
        Common.print("Stringed.play() " + n);
    }

    public String what() {
        return "Stringed";
    }

    public void adjust() {

    }
}

class Brass extends Wind {
    public void play(Note n) {
        Common.print("Brass.play() " + n);
    }

    public void adjust() {
        Common.print("Brass.adjust()");
    }
}

class Woodwind extends Wind {
    public void play(Note n) {
        Common.print("Woodwind.play() " + n);
    }

    public String what() {
        return "Woodwind";
    }
}

public class Music4 {
    static void tune(Instrument i) {
        i.play(Note.MIDDLE_C);
        i.adjust();
    }
    static void tuneAll(Instrument[] e) {
        for(Instrument i : e) {
            tune(i);
        }
    }

    public static void main(String[] args) {
        Instrument[] orchestra = {
                new Wind(),
                new Percussion(),
                new Stringed(),
                new Brass(),
                new Woodwind(),
        };
        tuneAll(orchestra);
    }
}
