package interfacesTest;

import java.util.Random;
import static util.Common.*;

public class RandomDouble {
    private Random rd = new Random(47);
    protected double next() {
        return rd.nextDouble();
    }

    public static void main(String[] args) {
        RandomDouble randomDouble = new RandomDouble();
        for (int i =0; i < 10; i++) {
            print(randomDouble.next() + " ");
        }
    }
}
